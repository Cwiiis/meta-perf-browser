DESCRIPTION = "ostree-provisioner"
HOMEPAGE = "https://gitlab.com/saavedra.pablo/ostree-provisioner"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d9d90ec8555e57d1d4b89f8e38fa54e2"

RDEPENDS_${PN} = "python3-requests python3-pytz python3-dateutil bash"
DEPENDS += "python3 ostree"

PV = "0.0.0+git${SRCPV}"
SRCREV = "6015cb74eb070e51440a991ecd6e2d3fb719e189"
SRC_URI = "git://gitlab.com/saavedra.pablo/ostree-provisioner.git;protocol=https;branch=master \
           file://ostree-provisioner.service \
           file://ostree-provisioner.cfg \
           file://ostree-provisioner-data.cfg \
           file://ostree-provisioner-init \
           file://ostree-provisioner-ok \
           file://ostree-provisioner-check \
         "

S = "${WORKDIR}/git"

inherit setuptools3 systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "${BPN}.service"

OSTREE_PROVISIONER_IMAGE_VERSION ?= "ostree_$(date +%Y%m%d%H%M%S)"

do_install_append () {
    install -d ${D}/${sysconfdir}/${BPN}
    install -m 600 ${WORKDIR}/ostree-provisioner.cfg ${D}/${sysconfdir}/${BPN}
    install -m 600 ${WORKDIR}/ostree-provisioner-data.cfg ${D}/${sysconfdir}/${BPN}
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/ostree-provisioner.service ${D}${systemd_unitdir}/system
    install -d ${D}/${bindir}
    install -D -m 0755 ${WORKDIR}/ostree-provisioner-init ${D}${bindir}
    install -D -m 0755 ${WORKDIR}/ostree-provisioner-ok ${D}${bindir}
    install -D -m 0755 ${WORKDIR}/ostree-provisioner-check ${D}${bindir}
    install -d ${D}/var/lib/ostree/
    echo ${OSTREE_PROVISIONER_IMAGE_VERSION} > ${D}/${sysconfdir}/${BPN}/version
}
