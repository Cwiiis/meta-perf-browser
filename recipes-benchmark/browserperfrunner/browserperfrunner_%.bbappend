FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://browserperfdash-benchmark.cfg"

SRCREV = "${AUTOREV}"

FILES_${PN} += "${sysconfdir}/browserperfdash-benchmark.cfg"

do_install_append() {
    install -d ${D}/${sysconfdir}/
    install -m 644 ${WORKDIR}/browserperfdash-benchmark.cfg ${D}/${sysconfdir}/
    install -d ${D}${PYTHON_SITEPACKAGES_DIR}/webkitpy/thirdparty
    install -d ${D}${PYTHON_SITEPACKAGES_DIR}/webkitpy/thirdparty/autoinstalled
}
