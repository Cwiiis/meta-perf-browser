FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

S = "${WORKDIR}/git"
SRC_URI = "git://github.com/WebKit/webkit.git;protocol=git;branch=master \
           file://keep_cplusplus_201703_check_in_StdLibExtras.patch \
"

PACKAGECONFIG[accessibility] = "-DENABLE_ACCESSIBILITY=ON,-DENABLE_ACCESSIBILITY=OFF,atk at-spi2-atk"

PV = "nightly-${SRCPV}"
SRCREV = "${AUTOREV}"

# FIXME: Trunk is borken and unstable since
# https://bugs.webkit.org/show_bug.cgi?id=203290. Fix the SRCREV in the
# latest revision with a build sucess reported temporary.
SRCREV = "66124254bbe1e8f111e4555e941b3536dea47143"

