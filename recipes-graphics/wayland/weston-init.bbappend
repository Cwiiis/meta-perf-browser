FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://weston.default \
            file://weston@.service"

inherit useradd
USERADD_PACKAGES = "${PN}"
GROUPADD_PARAM_${PN} = "--system bot; --system weston-launch; --system video; --system render"
USERADD_PARAM_${PN} = "--system -p '' -s /bin/bash -g bot -G weston-launch,video,render bot"


do_install_append () {
    install -d ${D}/${sysconfdir}/default/
    install -m 644 ${WORKDIR}/weston.default ${D}/${sysconfdir}/default/weston
}
