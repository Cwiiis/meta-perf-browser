FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

# Add egl/gles to config
# PACKAGECONFIG_append = " egl glesv2"
PACKAGECONFIG_append_rpi = " glesv2"
PACKAGECONFIG_append_mx6q = " glesv2"
PACKAGECONFIG_append_rpi = " egl"
PACKAGECONFIG_append_mx6q = " egl"
PACKAGECONFIG_append_use-mainline-bsp = " egl"
PACKAGECONFIG_append_use-mainline-bsp = " glesv2"
PACKAGECONFIG_remove_use-mainline-bsp = "opengl"
PACKAGECONFIG_append_class-target = " ${@bb.utils.contains('DISTRO_FEATURES', 'x11', '', 'glesv2', d)}"

SRC_URI += "file://0006-add-egl-device-create.patch"
SRC_URI += "file://0008-add-noaa-compositor.patch"
SRC_URI += "file://0009-error-check-just-in-debug.patch"
