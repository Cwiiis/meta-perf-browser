# adapted from linux-imx.inc, copyright (C) 2012-2014, 2017 O.S. Systems Software LTDA
# Released under the MIT license (see COPYING.MIT for the terms)

include linux-wandboard.inc

DEPENDS += "lzop-native bc-native"

SRCBRANCH = "wandboard_imx_4.9.11_1.0.0_ga"
SRCREV = "8cddbd39abd97b20c87a31c203e710bdc18af4ed"

COMPATIBLE_MACHINE = "(wandboard)"
