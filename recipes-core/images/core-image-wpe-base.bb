DESCRIPTION = "core-image-weston"

inherit image_browsers
inherit ${@bb.utils.contains('DISTRO_FEATURES','sota','image_browsers_ostree','', d)}

REQUIRED_DISTRO_FEATURES = "opengl wayland"

IMAGE_INSTALL_append = " weston"
IMAGE_INSTALL_append = " weston-init"

IMAGE_INSTALL_append = " wpebackend-fdo"


IMAGE_INSTALL_append = " packagegroup-wpewebkit-depends"
