DESCRIPTION = "core-image-wpe"

inherit image_browsers
inherit ${@bb.utils.contains('DISTRO_FEATURES','sota','image_browsers_ostree','', d)}


REQUIRED_DISTRO_FEATURES = "opengl"

IMAGE_INSTALL_append = " wpewebkit"
IMAGE_INSTALL_append = " wpebackend-rdk"
IMAGE_INSTALL_append = " cog"
